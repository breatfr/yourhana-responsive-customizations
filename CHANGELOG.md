# Changelog

All notable changes to this project will be documented in this file.

## Theme v1.1. - 2024-06-19
- update gitlab url

## Theme v1.1.0 - 2024-06-05
- put features as selectable options

## Theme v1.0.0 - 2024-02-03
- initial release
